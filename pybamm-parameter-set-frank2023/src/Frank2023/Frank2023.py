import pybamm
import os


def graphite_silicon_SDI48X_diffusivity_Frank2023(sto, T):
    """
    SDI 48X Silicon-graphite diffusivity as a function of temperature, in this case the
    diffusivity is taken to be a constant.

    Parameters
    ----------
    sto: :class:`pybamm.Symbol`
       Electrode stochiometry
    T: :class:`pybamm.Symbol`
       Dimensional temperature

    Returns
    -------
    :class:`pybamm.Symbol`
       Solid diffusivity
    """

    D_ref = 5e-13  # m^2/s, MA Unger
    E_D_s_R = 1202.8  # K, MA Unger
    arrhenius = pybamm.exp(E_D_s_R * (1 / 298.15 - 1 / T))
    return D_ref * arrhenius


def graphite_silicon_MJ1_electrolyte_exchange_current_density_Sturm2019(
    c_e, c_s_surf, c_s_max, T
):
    """
    Exchange-current density for Butler-Volmer reactions between graphite and LiPF6 in
    EC:DMC.

    References
    ----------
    J. Sturm, Everlasting electric vehicle enhanced range, lifetime and safety through ingenious battery management.
    doi:10.3030/713771

    Parameters
    ----------
    c_e : :class:`pybamm.Symbol`
        Electrolyte concentration [mol.m-3]
    c_s_surf : :class:`pybamm.Symbol`
        Particle concentration [mol.m-3]
    c_s_max : :class:`pybamm.Symbol`
        Maximum particle concentration [mol.m-3]
    T : :class:`pybamm.Symbol`
        Temperature [K]

    Returns
    -------
    :class:`pybamm.Symbol`
        Exchange-current density [A.m-2]
    """

    k = 3e-11  # MA Unger
    E_a_R = 3600  # K
    arrhenius = pybamm.exp(E_a_R * (1 / 298.15 - 1 / T))

    return (
        pybamm.constants.F * k * arrhenius * c_e**0.5 * c_s_surf**0.5 * (c_s_max - c_s_surf) ** 0.5
    )


def nca_SDI48X_diffusivity_Frank2023(sto, T):
    """
     NCA diffusivity as a function of stoichiometry, in this case the
     diffusivity is taken to be a constant.

     References
     ----------
     MA Unger

     Parameters
     ----------
     sto: :class:`pybamm.Symbol`
       Electrode stochiometry
     T: :class:`pybamm.Symbol`
        Dimensional temperature

     Returns
     -------
     :class:`pybamm.Symbol`
        Solid diffusivity
    """

    D_ref = 4e-14  # m^2/s, MA Unger
    E_D_s_R = 1202.8  # K, MA Unger
    arrhenius = pybamm.exp(E_D_s_R * (1 / 298.15 - 1 / T))

    return D_ref * arrhenius

def graphite_volume_change_Ai2020(sto, c_s_max):
    """
    Graphite particle volume change as a function of stochiometry [1, 2].

    References
    ----------
     .. [1] Ai, W., Kraft, L., Sturm, J., Jossen, A., & Wu, B. (2020).
     Electrochemical Thermal-Mechanical Modelling of Stress Inhomogeneity in
     Lithium-Ion Pouch Cells. Journal of The Electrochemical Society, 167(1), 013512
      DOI: 10.1149/2.0122001JES.
     .. [2] Rieger, B., Erhard, S. V., Rumpf, K., & Jossen, A. (2016).
     A new method to model the thickness change of a commercial pouch cell
     during discharge. Journal of The Electrochemical Society, 163(8), A1566-A1575.

    Parameters
    ----------
    sto: :class:`pybamm.Symbol`
        Electrode stochiometry, dimensionless
        should be R-averaged particle concentration
    Returns
    -------
    t_change:class:`pybamm.Symbol`
        volume change, dimensionless, normalised by particle volume
    """
    p1 = 145.907
    p2 = -681.229
    p3 = 1334.442
    p4 = -1415.710
    p5 = 873.906
    p6 = -312.528
    p7 = 60.641
    p8 = -5.706
    p9 = 0.386
    p10 = -4.966e-05
    t_change = (
        p1 * sto**9
        + p2 * sto**8
        + p3 * sto**7
        + p4 * sto**6
        + p5 * sto**5
        + p6 * sto**4
        + p7 * sto**3
        + p8 * sto**2
        + p9 * sto
        + p10
    )
    return t_change


def graphite_volume_change_Ai2020_derivative(sto, c_s_max):
    """
    Derivative of graphite particle volume change as a function of stochiometry [1, 2].

    References
    ----------
     .. [1] Ai, W., Kraft, L., Sturm, J., Jossen, A., & Wu, B. (2020).
     Electrochemical Thermal-Mechanical Modelling of Stress Inhomogeneity in
     Lithium-Ion Pouch Cells. Journal of The Electrochemical Society, 167(1), 013512
      DOI: 10.1149/2.0122001JES.
     .. [2] Rieger, B., Erhard, S. V., Rumpf, K., & Jossen, A. (2016).
     A new method to model the thickness change of a commercial pouch cell
     during discharge. Journal of The Electrochemical Society, 163(8), A1566-A1575.

    Parameters
    ----------
    sto: :class:`pybamm.Symbol`
        Electrode stochiometry, dimensionless
        should be R-averaged particle concentration
    Returns
    -------
    dt_change_dsto :class:`pybamm.Symbol`
        derivative of the volume change
    """
    p1 = 145.907
    p2 = -681.229
    p3 = 1334.442
    p4 = -1415.710
    p5 = 873.906
    p6 = -312.528
    p7 = 60.641
    p8 = -5.706
    p9 = 0.386
    dt_change_dsto = (
        9*p1*sto**8
        + 8*p2*sto**7
        + 7*p3*sto**6
        + 6*p4*sto**5
        + 5*p5*sto**4
        + 4*p6*sto**3
        + 3*p7*sto**2
        + 2*p8*sto
        + p9
    )
    return dt_change_dsto 


def volume_change_Ai2020(sto, c_s_max):
    """
    Particle volume change as a function of stochiometry [1, 2].

    References
    ----------
     .. [1] > Ai, W., Kraft, L., Sturm, J., Jossen, A., & Wu, B. (2020).
     Electrochemical Thermal-Mechanical Modelling of Stress Inhomogeneity in
     Lithium-Ion Pouch Cells. Journal of The Electrochemical Society, 167(1), 013512
      DOI: 10.1149/2.0122001JES.
     .. [2] > Rieger, B., Erhard, S. V., Rumpf, K., & Jossen, A. (2016).
     A new method to model the thickness change of a commercial pouch cell
     during discharge. Journal of The Electrochemical Society, 163(8), A1566-A1575.

    Parameters
    ----------
    sto: :class:`pybamm.Symbol`
        Electrode stochiometry, dimensionless
        should be R-averaged particle concentration
    Returns
    -------
    t_change:class:`pybamm.Symbol`
        volume change, dimensionless, normalised by particle volume
    """
    omega = pybamm.Parameter("Positive electrode partial molar volume [m3.mol-1]")
    t_change = omega * c_s_max * sto
    return t_change


def nmc_MJ1_electrolyte_exchange_current_density_Sturm2019(c_e, c_s_surf, c_s_max, T):
    """
    Exchange-current density for Butler-Volmer reactions between NCA and LiPF6 in
    EC:DMC.

    Parameters
    ----------
    c_e : :class:`pybamm.Symbol`
        Electrolyte concentration [mol.m-3]
    c_s_surf : :class:`pybamm.Symbol`
        Particle concentration [mol.m-3]
    T : :class:`pybamm.Symbol`
        Temperature [K]

    Returns
    -------
    :class:`pybamm.Symbol`
        Exchange-current density [A.m-2]
    """
    k = 1e-11  # m/s
    E_a_R = 3608.4  # K
    arrhenius = pybamm.exp(E_a_R * (1 / 298.15 - 1 / T))

    return (
        k * pybamm.constants.F * arrhenius * c_e**0.5 * c_s_surf**0.5 * (c_s_max - c_s_surf) ** 0.5
    )


def electrolyte_diffusivity_Valoen2005(c_e, T):
    """
    Diffusivity of 1M LiPF6 in PC/EC/DMC as a function of ion concentration and temperature. The data
    comes from [1]. 

    References
    ----------
    [1]	Valoen, L.O.; Reimers, J.N.: Transport Properties of LiPF6-Based Li-Ion Battery Electrolytes, 
    In: Journal of The Electrochemical Society (152), A882-A891. (2005). DOI: https://doi.org/10.1149/1.1872737

    Parameters
    ----------
    c_e: :class:`pybamm.Symbol`
        Dimensional electrolyte concentration
    T: :class:`pybamm.Symbol`
        Dimensional temperature

    Returns
    -------
    :class:`pybamm.Symbol`
        Solid diffusivity
    """
    c_e = c_e / 1000 # mol/m3 to mol/l
    D_c_e = 10e-4 * pybamm.exp(-4.43 - (54 / (T - 229 -5*c_e)) - 0.22 * c_e)
    D_c_e = D_c_e / (100*100) # cm2/s to m2/s
    return D_c_e


def electrolyte_conductivity_Valoen2005(c_e, T):
    """
    Conductivity of 1M LiPF6 in PC/EC/DMC as a function of ion concentration and temperature. The data
    comes from [1]. 

    References
    ----------
    [1]	Valoen, L.O.; Reimers, J.N.: Transport Properties of LiPF6-Based Li-Ion Battery Electrolytes, 
    In: Journal of The Electrochemical Society (152), A882-A891. (2005). DOI: https://doi.org/10.1149/1.1872737

    Parameters
    ----------
    c_e: :class:`pybamm.Symbol`
        Dimensional electrolyte concentration
    T: :class:`pybamm.Symbol`
        Dimensional temperature

    Returns
    -------
    :class:`pybamm.Symbol`
        Solid diffusivity
    """
    c_e = c_e / 1000 # mol/m^3 to mol/l
    sigma_e = 0.1*c_e * (-10.5 + 0.668 * c_e + 0.494 * c_e**2 + 0.074 * T - 0.0178 * c_e * T 
                         - 8.86e-4 * c_e**2 * T - 6.96e-5 * T + 2.8e-5 * c_e * T**2)
    return sigma_e


def electrolyte_TDF_Valoen2005(c_e, T):
    """
    Thermodynamic factor (TDF) of of 1M LiPF6 in PC/EC/DMC as a function of ion concentration and temperature. The data
    comes from [1]. 

    References
    ----------
    [1]	Valoen, L.O.; Reimers, J.N.: Transport Properties of LiPF6-Based Li-Ion Battery Electrolytes, 
    In: Journal of The Electrochemical Society (152), A882-A891. (2005). DOI: https://doi.org/10.1149/1.1872737

    Parameters
    ----------
    c_e: :class:`pybamm.Symbol`
        Dimensional electrolyte concentration
    T: :class:`pybamm.Symbol`
        Dimensional temperature

    Returns
    -------
    :class:`pybamm.Symbol`
        Electrolyte thermodynamic factor
    """
    c_e = c_e / 1000 # mol/m3 to mol/l
    caution_transference = 0.38
    tdf = (0.601 - 0.24 * c_e**0.5 + 0.983 * c_e**(3/2) * (1 - 0.0052 * (T - 294))) / (1-caution_transference)  
    return tdf


# Load data in the appropriate format
path, _ = os.path.split(os.path.abspath(__file__))
silicon_graphite_SDI48X_ocp_Frank2023_data = pybamm.parameters.process_1D_data(
    "silicon_graphite_SDI48X_ocp_Frank2023.csv", path=path
)


def silicon_graphite_SDI48X_ocp_Frank2023(sto):
    name, (x, y) = silicon_graphite_SDI48X_ocp_Frank2023_data
    return pybamm.Interpolant(x, y, sto, name=name, interpolator="cubic")


# Load data in the appropriate format
path, _ = os.path.split(os.path.abspath(__file__))
nca_SDI48X_ocp_Frank2023_data = pybamm.parameters.process_1D_data(
    "nca_SDI48X_ocp_Frank2023.csv", path=path
)


def nca_SDI48X_ocp_Frank2023(sto):
    name, (x, y) = nca_SDI48X_ocp_Frank2023_data
    return pybamm.Interpolant(x, y, sto, name=name, interpolator="cubic")


# Call dict via a function to avoid errors when editing in place
def get_parameter_values():
    """
    Parameters for an LG M50 cell, from the paper

        Simon E. J. O'Kane, Weilong Ai, Ganesh Madabattula, Diego Alonso-Alvarez, Robert
        Timms, Valentin Sulzer, Jacqueline Sophie Edge, Billy Wu, Gregory J. Offer, and
        Monica Marinescu. Lithium-ion battery degradation: how to model it. Phys. Chem.
        Chem. Phys., 24:7909-7922, 2022. URL: http://dx.doi.org/10.1039/D2CP00417H,
        doi:10.1039/D2CP00417H.


    based on the paper

        Chang-Hui Chen, Ferran Brosa Planella, Kieran O'Regan, Dominika Gastol, W.
        Dhammika Widanage, and Emma Kendrick. Development of Experimental Techniques for
        Parameterization of Multi-scale Lithium-ion Battery Models. Journal of The
        Electrochemical Society, 167(8):080534, 2020. doi:10.1149/1945-7111/ab9050.


    and references therein.

    Note: the SEI and plating parameters do not claim to be representative of the true
    parameter values. These are merely the parameter values that were used in the
    referenced papers.
    """

    return {
        "chemistry": "lithium_ion",
        # lithium plating (updated in config files)
        "Lithium metal partial molar volume [m3.mol-1]": 1.3e-05,
        "Lithium plating kinetic rate constant [m.s-1]": 1e-09,
        "Exchange-current density for plating [A.m-2]": 0,
        "Exchange-current density for stripping [A.m-2]": 0,
        "Initial plated lithium concentration [mol.m-3]": 0.0,
        "Typical plated lithium concentration [mol.m-3]": 1000.0,
        "Lithium plating transfer coefficient": 0.65,
        "Lithium plating tanh stretch [V-1]": 50,
        "Lithium plating tanh shift [V]": 50,
        # sei (updated in config files)
        "Ratio of lithium moles to SEI moles": 1.0,
        "Inner SEI reaction proportion": 0.0,
        "Inner SEI partial molar volume [m3.mol-1]": 9.585e-05,
        "Outer SEI partial molar volume [m3.mol-1]": 9.585e-05,
        "SEI reaction exchange current density [A.m-2]": 1.5e-07,
        "SEI resistivity [Ohm.m]": 200000.0,
        "Outer SEI solvent diffusivity [m2.s-1]": 2.5000000000000002e-22,
        "Bulk solvent concentration [mol.m-3]": 2636.0,
        "Inner SEI open-circuit potential [V]": 0.1,
        "Outer SEI open-circuit potential [V]": 0.8,
        "Inner SEI electron conductivity [S.m-1]": 8.95e-14,
        "Inner SEI lithium interstitial diffusivity [m2.s-1]": 1e-20,
        "Lithium interstitial reference concentration [mol.m-3]": 15.0,
        "Initial inner SEI thickness [m]": 0.0,
        "Initial outer SEI thickness [m]": 17.5e-09,
        "EC initial concentration in electrolyte [mol.m-3]": 4541.0,
        "EC diffusivity [m2.s-1]": 2e-18,
        "SEI on cracks scaling factor": 1,
        "SEI kinetic rate constant [m.s-1]": 0,  # no SEI growth
        "SEI open-circuit potential [V]": 0.4,
        "SEI growth activation energy [J.mol-1]": 38000.0,
        "Negative electrode reaction-driven LAM factor [m3.mol-1]": 0.0,
        "Positive electrode reaction-driven LAM factor [m3.mol-1]": 0.0,
        # cell
        "Negative electrode thickness [m]": 77e-06,  # MA Unger
        "Separator thickness [m]": 13.3e-06,  # MA Unger
        "Positive electrode thickness [m]": 61e-06,  # MA Unger
        "Electrode height [m]": 0.06285,  # MA Unger
        "Electrode width [m]": 2.159,  # MA Unger (2* 1.0795)
        "Cell volume [m3]": 2.42e-05,  # MA Unger
        "Cell thermal expansion coefficient [m.K-1]": 1.1e-06,
        "Nominal cell capacity [A.h]": 4.8,  # datasheet
        "Current function [A]": 4.8,  # datasheet
        "Contact resistance [Ohm]": 0.0, # only activates with model option "contact resistance": "true", but does not solve with current PyBaMM version
        # negative electrode
        "Negative electrode conductivity [S.m-1]": 100.0,  # MA Unger
        "Maximum concentration in negative electrode [mol.m-3]": 37295,  # MA Unger
        "Negative electrode diffusivity [m2.s-1]": graphite_silicon_SDI48X_diffusivity_Frank2023,  # MA Unger
        "Negative electrode OCP [V]": silicon_graphite_SDI48X_ocp_Frank2023,  # MA Unger
        "Negative electrode porosity": 0.35,  # MA Unger
        "Negative electrode active material volume fraction": 0.49386,  # MA Unger
        "Negative particle radius [m]": 6.0834e-06,  # MA Unger
        "Negative electrode Bruggeman coefficient (electrolyte)": 1.5,  # MA Unger
        "Negative electrode Bruggeman coefficient (electrode)": 1.5,  # MA Unger
        "Negative electrode charge transfer coefficient": 0.5,  # MA Unger
        "Negative electrode double-layer capacity [F.m-2]": 0.2,
        "Negative electrode exchange-current density [A.m-2]"
        "": graphite_silicon_MJ1_electrolyte_exchange_current_density_Sturm2019,  # MA Unger
        "Negative electrode density [kg.m-3]": 2300.0,  # MA Unger
        "Negative electrode specific heat capacity [J.kg-1.K-1]": 659.4,  # MA Unger
        "Negative electrode OCP entropic change [V.K-1]": 0.0,
        # negative electrode cracking parameters (from OKane2022 dataset - updated in config files)
        "Negative electrode Poisson's ratio": 0.3,
        "Negative electrode Young's modulus [Pa]": 15000000000.0,
        "Negative electrode reference concentration for free of deformation [mol.m-3]": 0.0,
        "Negative electrode partial molar volume [m3.mol-1]": 3.1e-06,
        "Negative electrode volume change": graphite_volume_change_Ai2020,
        "Negative electrode volume change derivative": graphite_volume_change_Ai2020_derivative,
        "Negative electrode Paris' law constant b": 1.12,
        "Negative electrode Paris' law constant m": 2.2,
        "Negative electrode LAM constant proportional term [s-1]": 2.7778e-07,
        "Negative electrode LAM constant exponential term": 2.0,
        "Negative electrode critical stress [Pa]": 60000000.0,
        # positive electrode
        "Positive electrode conductivity [S.m-1]": 1.2,  # MA Unger
        "Maximum concentration in positive electrode [mol.m-3]": 48452,  # MA Unger
        "Positive electrode diffusivity [m2.s-1]": nca_SDI48X_diffusivity_Frank2023,  # MA Unger
        "Positive electrode OCP [V]": nca_SDI48X_ocp_Frank2023, # MA Unger
        "Positive electrode porosity": 0.2,  # MA Unger
        "Positive electrode active material volume fraction": 0.70104,  # MA Unger
        "Positive particle radius [m]": 4.642e-06,  # MA Unger
        "Positive electrode Bruggeman coefficient (electrolyte)": 1.85,  # MA Unger
        "Positive electrode Bruggeman coefficient (electrode)": 1.85,  # MA Unger
        "Positive electrode charge transfer coefficient": 0.5,  # MA Unger
        "Positive electrode double-layer capacity [F.m-2]": 0.2,
        "Positive electrode exchange-current density [A.m-2]"
        "": nmc_MJ1_electrolyte_exchange_current_density_Sturm2019,  # MA Unger
        "Positive electrode density [kg.m-3]": 4700,  # MA Unger
        "Positive electrode specific heat capacity [J.kg-1.K-1]": 659.4,  # MA Unger
        "Positive electrode OCP entropic change [V.K-1]": 0.0,
        # positive electrode cracking parameters (from OKane2022 dataset - updated in config files)
        "Positive electrode Poisson's ratio": 0.2,
        "Positive electrode Young's modulus [Pa]": 375000000000.0,
        "Positive electrode reference concentration for free of deformation [mol.m-3]": 0.0,
        "Positive electrode partial molar volume [m3.mol-1]": 1.25e-05,
        "Positive electrode volume change": volume_change_Ai2020,
        "Positive electrode volume change derivative": 0,
        "Positive electrode Paris' law constant b": 1.12,
        "Positive electrode Paris' law constant m": 2.2,
        "Positive electrode LAM constant proportional term [s-1]": 2.7778e-07,
        "Positive electrode LAM constant exponential term": 2.0,
        "Positive electrode critical stress [Pa]": 375000000.0,
        # separator
        "Separator porosity": 0.45,  # MA Unger
        "Separator Bruggeman coefficient (electrolyte)": 1.5,  # MA Unger
        "Separator density [kg.m-3]": 397.0,
        "Separator specific heat capacity [J.kg-1.K-1]": 700.0,
        "Separator thermal conductivity [W.m-1.K-1]": 0.16,
        # electrolyte
        "Initial concentration in electrolyte [mol.m-3]": 1000.0, # MA Unger
        "Cation transference number": 0.38,  # MA Unger
        "Thermodynamic factor": electrolyte_TDF_Valoen2005,  # MA Unger
        "Electrolyte diffusivity [m2.s-1]"
        "": electrolyte_diffusivity_Valoen2005, # MA Unger
        "Electrolyte conductivity [S.m-1]"
        "": electrolyte_conductivity_Valoen2005, # MA Unger
        # experiment
        "Reference temperature [K]": 298.15,
        "Total heat transfer coefficient [W.m-2.K-1]": 10.0,
        "Ambient temperature [K]": 298.15,
        "Number of electrodes connected in parallel to make a cell": 1.0,
        "Number of cells connected in series to make a battery": 1.0,
        "Lower voltage cut-off [V]": 2.5,
        "Upper voltage cut-off [V]": 4.2,
        "Open-circuit voltage at 0% SOC [V]": 2.5,
        "Open-circuit voltage at 100% SOC [V]": 4.2,
        "Initial concentration in negative electrode [mol.m-3]": 1288.5,  # calculated 0.03455 * 37295 mol.m-3
        "Initial concentration in positive electrode [mol.m-3]": 46029.4,  # calculated 0.95 * 48452 mol.m-3
        "Initial temperature [K]": 298.15,
        "citations": ["Frank2023"],
    }
