import multiprocessing
import os
import time
from configparser import ConfigParser
from datetime import datetime
from multiprocessing import Queue

from simses.commons.console_printer import ConsolePrinter
from simses.commons.utils.utilities import format_float
from simses.commons.config.generation.simulation import SimulationConfigGenerator

from main import OptSimMPC


class OptSimMPCBatchProcessing:

    """
    BatchProcessing delivers the necessary handling for running multiple parallel OptSimMPC simulations and distributes
    the configuration to each process. It supervises all processes and starts as many processes as there are cores
    available. If you run more simulations than cores available, it waits until a simulation has finished and fills
    the gap with new simulation processes.
    """

    UPDATE: int = 1  # s

    def __init__(self, config_set: dict, analysis_config: ConfigParser, path: str = os.getcwd()):
        # self.__max_parallel_processes: int = max(1, multiprocessing.cpu_count() - 1)
        self.__max_parallel_processes: int = 15
        self.__path: str = os.path.join(path, 'results_batch').replace('\\','/') + '/'
        print('Results will be stored in ' + self.__path)
        self.__sim_opt_config_set: dict = config_set
        self.__analysis_config: ConfigParser = analysis_config

    def run(self):
        print(str(len(self.__sim_opt_config_set)) + ' simulations configured')
        printer_queue: Queue = Queue(maxsize=len(self.__sim_opt_config_set) * 2)
        printer: ConsolePrinter = ConsolePrinter(printer_queue)
        jobs: [OptSimMPC] = list()
        for key, value in self.__sim_opt_config_set.items():
            jobs.append(OptSimMPC(config_sim=value[0], config_opt=value[1], config_analysis=self.__analysis_config,
                                  path=self.__path, name=key))
        printer.start()
        started: [OptSimMPC] = list()
        start = time.time()
        job_count: int = len(jobs)
        count: int = 0
        for job in jobs:
            job.start()
            started.append(job)
            count += 1
            print('\rStarting simulation ' + str(count) + '/' + str(job_count) + ' @ ' + str(datetime.now()))
            self.__check_running_process(started)
        for job in jobs:
            job.join()
        duration: float = (time.time() - start) / 60.0
        print('\r\n' + type(self).__name__ + ' finished ' + str(job_count) + ' simulations in ' + format_float(duration) + ' min '
              '(' + format_float(duration / job_count) + ' min per simulation)')
        printer.stop_immediately()

    def __check_running_process(self, processes: [multiprocessing.Process]) -> None:
        while True:
            running_jobs: int = 0
            for process in processes:
                if process.is_alive():
                    running_jobs += 1
            if running_jobs < self.__max_parallel_processes:
                break
            time.sleep(self.UPDATE)


if __name__ == "__main__":

    opt = "MpcOptArbitrageOnePrice"
    conf_set = {}

    # aging_cost = 538  # optimum for 2021 (Collath et al. 2023)
    aging_cost = 1225  # optimum for 2022 (Collath et al. 2023)
    capacity_idm = 1.076e6
    start_soc = 0.5

    # batteries = ["SamsungNCA21700", "SamsungNMC94AhLatelifestudy", "SonyLFP"]
    batteries = ["SamsungNCA21700", "SamsungNMC94AhLatelifestudy"]
    soh_values = [1, 0.95, 0.9, 0.85, 0.8, 0.75, 0.7, 0.65, 0.60, 0.55, 0.50]
    # soh_values = [1, 0.95]
    rinc_values = [0.6245/(0.9829-0.7380)*(1-soh) for soh in soh_values]  # based on SamsungNCA21700 aging study

    def config_helper(battery_name, soh_value, resistance_increase):
        conf_sim = ConfigParser()
        conf_sim.read("configs/simulation.local_latelife_part1_IDM.ini")
        conf_sim["STORAGE_SYSTEM"]["STORAGE_TECHNOLOGY"] = "\n"+"technology_1, "+str(capacity_idm)+", lithium_ion, " + battery_name + ", " + str(start_soc) +", "+str(soh_value)
        conf_sim["BATTERY"]["START_RESISTANCE_INC"] = "{:.2f}".format(resistance_increase)

        conf_opt = ConfigParser()
        conf_opt.read("configs/optimization.optsim.ini")
        conf_opt["GENERAL"]["AGING_COST"] = str(aging_cost)

        return conf_sim, conf_opt

    for battery in batteries:
        # for soh in soh_values:
        #     rinc = 0
        #     name = "LateLifePart1_IDM" + "_" + "{:03.0f}".format(soh * 100) + "_" + "{:03.0f}".format(
        #         rinc * 100) + "_" + battery
        #     conf_set[name] = config_helper(battery, soh, rinc)
        # for rinc in rinc_values:
        #     soh = 1
        #     name = "LateLifePart1_IDM" + "_" + "{:03.0f}".format(soh * 100) + "_" + "{:03.0f}".format(
        #         rinc * 100) + "_" + battery
        #     conf_set[name] = config_helper(battery, soh, rinc)
        for (soh, rinc) in zip(soh_values, rinc_values):
            name = "LateLifePart1_IDM" + "_" + "{:03.0f}".format(soh * 100) + "_" + "{:03.0f}".format(
                rinc * 100) + "_" + battery
            conf_set[name] = config_helper(battery, soh, rinc)

    conf_analysis = ConfigParser()
    conf_analysis.read("configs/analysis.optsim.ini")

    start = time.time()
    batch_processing = OptSimMPCBatchProcessing(config_set=conf_set, analysis_config=conf_analysis)
    batch_processing.run()
    end = time.time()
    print("Execution time is: ", end-start, " seconds.")

