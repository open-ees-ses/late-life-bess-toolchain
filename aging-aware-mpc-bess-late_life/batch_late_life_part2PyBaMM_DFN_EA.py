import multiprocessing
import os
import time
from configparser import ConfigParser
from datetime import datetime
from multiprocessing import Queue

from simses.commons.console_printer import ConsolePrinter
from simses.commons.utils.utilities import format_float

from main import OptSimMPC


class OptSimMPCBatchProcessing:

    """
    BatchProcessing delivers the necessary handling for running multiple parallel OptSimMPC simulations and distributes
    the configuration to each process. It supervises all processes and starts as many processes as there are cores
    available. If you run more simulations than cores available, it waits until a simulation has finished and fills
    the gap with new simulation processes.
    """

    UPDATE: int = 1  # s

    def __init__(self, config_set: dict, analysis_config: ConfigParser, path: str = os.getcwd()):
        # self.__max_parallel_processes: int = max(1, multiprocessing.cpu_count() - 1)
        self.__max_parallel_processes: int = 15
        self.__path: str = os.path.join(path, 'results_batch').replace('\\','/') + '/'
        print('Results will be stored in ' + self.__path)
        self.__sim_opt_config_set: dict = config_set
        self.__analysis_config: ConfigParser = analysis_config

    def run(self):
        print(str(len(self.__sim_opt_config_set)) + ' simulations configured')
        printer_queue: Queue = Queue(maxsize=len(self.__sim_opt_config_set) * 2)
        printer: ConsolePrinter = ConsolePrinter(printer_queue)
        jobs: [OptSimMPC] = list()
        for key, value in self.__sim_opt_config_set.items():
            jobs.append(OptSimMPC(config_sim=value[0], config_opt=value[1], config_analysis=self.__analysis_config,
                                  path=self.__path, name=key))
        printer.start()
        started: [OptSimMPC] = list()
        start = time.time()
        job_count: int = len(jobs)
        count: int = 0
        for job in jobs:
            job.start()
            started.append(job)
            count += 1
            print('\rStarting simulation ' + str(count) + '/' + str(job_count) + ' @ ' + str(datetime.now()))
            self.__check_running_process(started)
        for job in jobs:
            job.join()
        duration: float = (time.time() - start) / 60.0
        print('\r\n' + type(self).__name__ + ' finished ' + str(job_count) + ' simulations in ' + format_float(duration) + ' min '
              '(' + format_float(duration / job_count) + ' min per simulation)')
        printer.stop_immediately()

    def __check_running_process(self, processes: [multiprocessing.Process]) -> None:
        while True:
            running_jobs: int = 0
            for process in processes:
                if process.is_alive():
                    running_jobs += 1
            if running_jobs < self.__max_parallel_processes:
                break
            time.sleep(self.UPDATE)


if __name__ == "__main__":
    opt = "MpcOptArbitrageOnePrice"

    # aging_cost = 538  # optimum for 2021 (Collath et al. 2023)
    aging_cost = 1225  # optimum for 2022 (Collath et al. 2023)

    def config_helper() -> dict:
        # returns dictionary with tuples of simulation and optimization config
        config_paths = ["simulation.local_20230929_IDM_ParameterSetV6.ini"]

        # config set to be later passed to SimSES
        config_set: dict = dict()

        # read opt config
        # read sim configs
        for config_name in config_paths:
            conf_sim = ConfigParser()
            conf_sim.read("configs/"+config_name)
            conf_sim["LIB_PYBAMM"]["MODEL_TYPE"] = "DFN"
            conf_opt = ConfigParser()
            conf_opt.read("configs/optimization.optsim.ini")
            conf_opt["GENERAL"]["AGING_COST"] = str(aging_cost)
            name = config_name[-18:] + " - DFN - basic"
            config_set[name] = (conf_sim, conf_opt)

            conf_sim = ConfigParser()
            conf_sim.read("configs/" + config_name)
            conf_sim["LIB_PYBAMM"]["MODEL_TYPE"] = "DFN"
            name = config_name[-18:] + " - DFN - adapted all"
            conf_opt = ConfigParser()
            conf_opt.read("configs/optimization.optsim.ini")
            conf_opt["ADAPTED_OPERATING_CONDITIONS"]["ADAPTED_OPERATING_CONDITIONS"] = "True"
            conf_opt["GENERAL"]["AGING_COST"] = str(aging_cost)
            config_set[name] = (conf_sim, conf_opt)

            # conf_sim = ConfigParser()
            # conf_sim.read("configs/" + config_name)
            # conf_sim["LIB_PYBAMM"]["MODEL_TYPE"] = "DFN"
            # name = config_name[-18:] + " - DFN - adapted charge rate"
            # conf_opt = ConfigParser()
            # conf_opt.read("configs/optimization.optsim.ini")
            # conf_opt["ADAPTED_OPERATING_CONDITIONS"]["ADAPTED_OPERATING_CONDITIONS"] = "True"
            # conf_opt["ADAPTED_OPERATING_CONDITIONS"]["SOC_MAX"] = str(1)
            # conf_opt["ADAPTED_OPERATING_CONDITIONS"]["SOC_MIN"] = str(0)
            # conf_opt["GENERAL"]["AGING_COST"] = str(aging_cost)
            # config_set[name] = (conf_sim, conf_opt)
        return config_set

    conf_set = config_helper()
    conf_analysis = ConfigParser()
    conf_analysis.read("configs/analysis.optsim.ini")
    start = time.time()
    batch_processing = OptSimMPCBatchProcessing(config_set=conf_set, analysis_config=conf_analysis)
    batch_processing.run()
    end = time.time()
    print("Execution time is: ", end-start, " seconds.")
