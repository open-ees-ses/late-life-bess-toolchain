# Suitability of late-life lithium-ion cells for battery energy storage systems
This repository contains the toolchain that was used in a related publication.

## Setup and installation

### 1. Create a virtual environment
Create a virtual environment, for example with either
[venv](https://packaging.python.org/en/latest/guides/installing-using-pip-and-virtual-environments/#creating-a-virtual-environment) or [conda](https://docs.conda.io/projects/conda/en/latest/user-guide/tasks/manage-environments.html), or directly through your IDE.

### 2. Install dependencies
Install `simses` and all other required python packages in your virtual environment. This can be done with a single command with the provided "requirements" file:
```
pip install -r requirements.txt
```
Some IDEs, like [PyCharm](https://www.jetbrains.com/help/pycharm/creating-virtual-environment.html#env-requirements) and [VS Code](https://code.visualstudio.com/docs/python/environments) 
may install the requirements automatically when setting up the virtual environment.

### 3. Install tools of this toolchain as local packages
Run the below code to install the individual tools as local packages:
```
pip install -e simses-late-life
pip install -e pybamm-late_life
pip install -e pybamm-parameter-set-frank2023
```

## Project structure
Overview of the tools and packages used:
- `aging-aware-mpc-bess-late_life`: This tool runs an optimization model on top of SimSES for energy arbitrage on the intraday electricity market,
using a model predictive control approach. It has been adapted for this toolchain to allow changing the operating conditions after a certain SOH value has been reached. 
Furthermore, it includes the files to run the batch simulation for the energy arbitrage application of the associated publication. 
  - This tool has previously been published here: https://gitlab.lrz.de/open-ees-ses/aging-aware-MPC. 
  - It has been described in detail in this contribution: https://doi.org/10.1016/j.apenergy.2023.121531. 
  - All currently implemented optimization models use the Gurobi solver and API.
  Gurobi is a commercial solver, but free licenses are available for academic use. 
  Academic licenses for Gurobi are available[ here](https://www.gurobi.com/features/academic-named-user-license/).
- `pybamm-late_life`: Adapted version of PyBaMM based on PyBaMM release 23.5 (https://github.com/pybamm-team/PyBaMM).
In particular, the lithium plating reaction and SEI formation was adapted as described in the associated paper.
- `pybamm-parameter-set-frank2023`: P2D parameter set for the 4.8 Ah cylindrical cell used in the 
associated paper.
- `simses-late-life`: Adapted version of SimSES based on SimSES release 1.3.5 (https://gitlab.lrz.de/open-ees-ses/simses).
In particular, this version includes the module (`simses-late-life/simses/technology/lib_pybamm`) that allows to perform battery cell simulations in SimSES, while
the upstream BESS components and analysis functions from SimSES are used. Furthermore equivalent circuit models for the 94 Ah prismatic
and 4.8 Ah cylindrical cell from the associated paper are included in this version of SimSES.

## Batch simulations
Pre-configured batch-simulation files are available to run the same simulations that were conducted for the associated paper:
- `late-life-bess-paper-toolchain\simses-late-life\simses\simulation\batch_late_life_part1_ECM_SCI.py`: Used to run the simulations with the ECM model
of part 1 of the results for the self-consumption increase application.
- `late-life-bess-paper-toolchain\simses-late-life\simses\simulation\batch_late_life_part2PyBaMM_DFN_SCI.py`: Used to run the simulations with the P2D model
in PyBaMM of part 2 of the results for the self-consumption increase applications.
- `late-life-bess-paper-toolchain\aging-aware-mpc-bess-late_life\batch_late_life_part1_ECM_EA.py`: Used to run the simulations with the ECM model
of part 1 of the results for the energy arbitrage application.
- `late-life-bess-paper-toolchain\aging-aware-mpc-bess-late_life\batch_late_life_part2PyBaMM_DFN_EA.py`: Used to run the simulations with the P2D model
in PyBaMM of part 2 of the results for the energy arbitrage application; both for the standard energy arbitrage application and with adapted operating conditions.

