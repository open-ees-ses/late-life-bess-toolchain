from configparser import ConfigParser

from simses.commons.config.simulation.simulation_config import SimulationConfig
import ast


class LibPyBaMMConfig(SimulationConfig):
    """
    Battery specific configs
    """

    def __init__(self, config: ConfigParser, path: str = None):
        super().__init__(path, config)
        self.__section: str = 'LIB_PYBAMM'

    @property
    def save_pybamm_model(self) -> bool:
        """Returns start soc from data_config file_name"""
        return self.get_property(self.__section, 'SAVE_PYBAMM_MODEL') in ['True']

    @property
    def model_type(self) -> str:
        """Returns start soc from data_config file_name"""
        return str(self.get_property(self.__section, 'MODEL_TYPE'))

    @property
    def model_options(self) -> dict:
        """Returns dictionary of model options from data_config file_name"""
        return ast.literal_eval(self.get_property(self.__section, 'MODEL_OPTIONS'))

    @property
    def parameter_set(self) -> str:
        """Returns dictionary of chosen parameter set from data_config file_name"""
        return str(self.get_property(self.__section, 'PARAMETER_SET'))

    @property
    def checkup_frequency(self) -> int:
        """Returns an int of the selected frequency for performing checkups"""
        return int(self.get_property(self.__section, 'CHECKUP_FREQUENCY'))

    @property
    def parameter_update_1(self) -> dict:
        """Returns dictionary of update parameters from data_config file_name"""
        return ast.literal_eval(self.get_property(self.__section, 'PARAMETER_UPDATE_1'))

    @property
    def parameter_update_2(self) -> dict:
        """Returns dictionary of update parameters from data_config file_name"""
        return ast.literal_eval(self.get_property(self.__section, 'PARAMETER_UPDATE_2'))

    @property
    def parameter_update_3(self) -> dict:
        """Returns dictionary of update parameters from data_config file_name"""
        return ast.literal_eval(self.get_property(self.__section, 'PARAMETER_UPDATE_3'))
