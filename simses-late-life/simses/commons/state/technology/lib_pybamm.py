from simses.commons.state.technology.storage import StorageTechnologyState


class LibPyBaMMState(StorageTechnologyState):
    """
    Current physical state of the lithium_ion with the main electrical parameters.
    """

    SYSTEM_AC_ID: str = 'StorageSystemAC'
    SYSTEM_DC_ID: str = 'StorageSystemDC'
    SOC: str = 'SOC in p.u.'
    SOE: str = 'State of energy in Wh'
    SOH: str = 'State of health in p.u.'
    VOLTAGE: str = 'Voltage in V'
    NOMINAL_VOLTAGE: str = 'Nominal voltage in V'
    CURRENT: str = 'Current in A'
    TEMPERATURE: str = 'Temperature in K'
    CAPACITY: str = 'Capacity in Wh'
    POWER_LOSS: str = 'P_loss in W'
    FULFILLMENT: str = 'Bat_ful in p.u.'
    LLI: str = 'LLI in %'
    LLI_SEI: str = 'LLI to SEI in mol'
    LLI_SEI_CRACK: str = 'LLI to SEI on cracks in mol'
    LLI_PLATING: str = 'LLI to LI plating in mol'
    LLI_LAM_NE: str = 'LLI to LAM_NE in mol'
    LLI_LAM_PE: str = 'LLI to LAM_PE in mol'
    LAM_NE: str = 'LAM_NE in %'
    LAM_PE: str = 'LAM_PE in %'
    TOTAL_SEI_THICKNESS = 'Avg. total SEI in m'
    LI_PLATING_THICKNESS = "Avg. Li plating in m"
    ANODE_POROSITY = 'Avg. anode porosity in p.u.'
    ANODE_POROSITY_MIN = 'Min. anode porosity in p.u.'

    MAX_CHARGE_POWER: str = 'Maximum charging power in W'
    MAX_DISCHARGE_POWER: str = 'Maximum discharging power in W'
    INTERNAL_RESISTANCE: str = 'Internal resistance in Ohm'
    RESISTANCE_INCREASE: str = 'R increase in p.u.'

    def __init__(self, system_id: int, storage_id: int):
        super().__init__()
        self._initialize()
        self.set(self.SYSTEM_AC_ID, system_id)
        self.set(self.SYSTEM_DC_ID, storage_id)

    @property
    def soc(self) -> float:
        return self.get(self.SOC)

    @soc.setter
    def soc(self, value: float) -> None:
        self.set(self.SOC, value)

    @property
    def lli(self) -> float:
        return self.get(self.LLI)

    @lli.setter
    def lli(self, value: float) -> None:
        self.set(self.LLI, value)

    @property
    def lli_sei(self) -> float:
        return self.get(self.LLI_SEI)

    @lli_sei.setter
    def lli_sei(self, value: float) -> None:
        self.set(self.LLI_SEI, value)

    @property
    def lli_sei_crack(self) -> float:
        return self.get(self.LLI_SEI_CRACK)

    @lli_sei_crack.setter
    def lli_sei_crack(self, value: float) -> None:
        self.set(self.LLI_SEI_CRACK, value)

    @property
    def lli_plating(self) -> float:
        return self.get(self.LLI_PLATING)

    @lli_plating.setter
    def lli_plating(self, value: float) -> None:
        self.set(self.LLI_PLATING, value)

    @property
    def lli_lam_ne(self) -> float:
        return self.get(self.LLI_LAM_NE)

    @lli_lam_ne.setter
    def lli_lam_ne(self, value: float) -> None:
        self.set(self.LLI_LAM_NE, value)

    @property
    def lli_lam_pe(self) -> float:
        return self.get(self.LLI_LAM_PE)

    @lli_lam_pe.setter
    def lli_lam_pe(self, value: float) -> None:
        self.set(self.LLI_LAM_PE, value)

    @property
    def lam_ne(self) -> float:
        return self.get(self.LAM_NE)

    @lam_ne.setter
    def lam_ne(self, value: float) -> None:
        self.set(self.LAM_NE, value)

    @property
    def lam_pe(self) -> float:
        return self.get(self.LAM_PE)

    @lam_pe.setter
    def lam_pe(self, value: float) -> None:
        self.set(self.LAM_PE, value)

    @property
    def total_sei_thickness(self) -> float:
        return self.get(self.TOTAL_SEI_THICKNESS)

    @total_sei_thickness.setter
    def total_sei_thickness(self, value: float) -> None:
        self.set(self.TOTAL_SEI_THICKNESS, value)

    @property
    def li_plating_thickness(self) -> float:
        return self.get(self.LI_PLATING_THICKNESS)

    @li_plating_thickness.setter
    def li_plating_thickness(self, value: float) -> None:
        self.set(self.LI_PLATING_THICKNESS, value)

    @property
    def anode_porosity(self) -> float:
        return self.get(self.ANODE_POROSITY)

    @anode_porosity.setter
    def anode_porosity(self, value: float) -> None:
        self.set(self.ANODE_POROSITY, value)

    @property
    def anode_porosity_min(self) -> float:
        return self.get(self.ANODE_POROSITY_MIN)

    @anode_porosity_min.setter
    def anode_porosity_min(self, value: float) -> None:
        self.set(self.ANODE_POROSITY_MIN, value)

    @property
    def soe(self) -> float:
        return self.get(self.SOE)

    @soe.setter
    def soe(self, value: float) -> None:
        self.set(self.SOE, value)

    @property
    def soh(self) -> float:
        return self.get(self.SOH)

    @soh.setter
    def soh(self, value: float) -> None:
        self.set(self.SOH, value)

    @property
    def voltage(self) -> float:
        return self.get(self.VOLTAGE)

    @voltage.setter
    def voltage(self, value: float) -> None:
        self.set(self.VOLTAGE, value)

    @property
    def nominal_voltage(self) -> float:
        return self.get(self.NOMINAL_VOLTAGE)

    @nominal_voltage.setter
    def nominal_voltage(self, value: float) -> None:
        self.set(self.NOMINAL_VOLTAGE, value)

    @property
    def current(self) -> float:
        return self.get(self.CURRENT)

    @current.setter
    def current(self, value: float) -> None:
        self.set(self.CURRENT, value)

    @property
    def temperature(self) -> float:
        return self.get(self.TEMPERATURE)

    @temperature.setter
    def temperature(self, value: float) -> None:
        self.set(self.TEMPERATURE, value)

    @property
    def capacity(self) -> float:
        return self.get(self.CAPACITY)

    @capacity.setter
    def capacity(self, value: float) -> None:
        self.set(self.CAPACITY, value)

    @property
    def power_loss(self) -> float:
        return self.get(self.POWER_LOSS)

    @power_loss.setter
    def power_loss(self, value: float) -> None:
        self.set(self.POWER_LOSS, value)

    @property
    def fulfillment(self) -> float:
        return self.get(self.FULFILLMENT)

    @fulfillment.setter
    def fulfillment(self, value: float) -> None:
        self.set(self.FULFILLMENT, value)

    @property
    def id(self) -> str:
        return 'LIION' + str(self.get(self.SYSTEM_AC_ID)) + str(self.get(self.SYSTEM_DC_ID))

    @property
    def is_charge(self) -> bool:
        return self.current > 0

    @property
    def max_charge_power(self) -> float:
        return self.get(self.MAX_CHARGE_POWER)

    @max_charge_power.setter
    def max_charge_power(self, value: float) -> None:
        self.set(self.MAX_CHARGE_POWER, value)

    @property
    def max_discharge_power(self) -> float:
        return self.get(self.MAX_DISCHARGE_POWER)

    @max_discharge_power.setter
    def max_discharge_power(self, value: float) -> None:
        self.set(self.MAX_DISCHARGE_POWER, value)

    @classmethod
    def sum_parallel(cls, states: []):
        pass

    @classmethod
    def sum_serial(cls, states: []):
        pass

    @property
    def internal_resistance(self) -> float:
        return self.get(self.INTERNAL_RESISTANCE)

    @internal_resistance.setter
    def internal_resistance(self, value: float) -> None:
        self.set(self.INTERNAL_RESISTANCE, value)

    @property
    def resistance_increase(self) -> float:
        return self.get(self.RESISTANCE_INCREASE)

    @resistance_increase.setter
    def resistance_increase(self, value: float) -> None:
        self.set(self.RESISTANCE_INCREASE, value)
