from simses.commons.config.simulation.general import GeneralSimulationConfig
from simses.commons.data.data_handler import DataHandler
from simses.commons.log import Logger
from simses.commons.state.technology.lib_pybamm import LibPyBaMMState
from simses.commons.state.technology.storage import StorageTechnologyState
from simses.system.auxiliary.auxiliary import Auxiliary
from simses.technology.storage import StorageTechnology
from simses.commons.config.simulation.lib_pybamm import LibPyBaMMConfig
import numpy as np

import pybamm
pybamm.set_logging_level("NOTICE")


class LibPybaMM(StorageTechnology):
    """Uses PyBaMM within the SimSES simulation environment"""

    def __init__(self, voltage: float, capacity: float, data_export: DataHandler,
                 storage_id: int, system_id: int, config_general: GeneralSimulationConfig,
                 config_pybamm: LibPyBaMMConfig):
        super().__init__()
        self.__log: Logger = Logger(type(self).__name__)
        self.__data_export: DataHandler = data_export

        # Fixed settings
        self.__save_last_state_only = not config_pybamm.save_pybamm_model
        self.__checkup_frequency = config_pybamm.checkup_frequency
        self.__counter_checkup = 0
        self.__counter_failed_checkup_c = 0
        self.__counter_failed_checkup_r = 0
        self.__voltage_CV_rounding = 1  # to ensure max. voltage is not exceeded at start of simulation
        self.__var_pts = {
            "x_n": 20,  # negative electrode
            "x_s": 20,  # separator
            "x_p": 20,  # positive electrode
            "r_n": 20,
            "r_p": 20,  # positive particle, n_Shells
        }
        self.__solver = pybamm.CasadiSolver(max_step_decrease_count=10)

        # PyBaMM parameters
        parameter_set = config_pybamm.parameter_set
        # Entries: [Capacity CC 1C Wh, Capacity CC 1C Ah, Nominal Voltage V]
        # Determined through separate PyBaMM experiment and required to scale up cell to system in SimSES
        dict_nominal_cell_parameter = {"Mohtat2020": [17.90, 4.84, 3.67],
                                       "Frank2023": [17.47, 4.8, 3.64]}
        if parameter_set not in dict_nominal_cell_parameter.keys():
            print("Add nominal capacity Wh, nominal capacity Ah and nominal voltage for the selected parameter set.")
        self.__parameter_values = pybamm.ParameterValues(parameter_set)
        self.__parameter_values.update(config_pybamm.parameter_update_1)
        self.__parameter_values.update(config_pybamm.parameter_update_2)
        self.__parameter_values.update(config_pybamm.parameter_update_3)

        # create model
        if config_pybamm.model_type == "SPM":
            self.__model = pybamm.lithium_ion.SPM(config_pybamm.model_options)
        elif config_pybamm.model_type == "SPMe":
            self.__model = pybamm.lithium_ion.SPMe(config_pybamm.model_options)
        elif config_pybamm.model_type == "DFN":
            self.__model = pybamm.lithium_ion.DFN(config_pybamm.model_options)

        # create initial state
        self.__state = LibPyBaMMState(system_id=system_id, storage_id=storage_id)
        self.__log.debug('created: ' + str(self.__state))
        self.__state.nominal_voltage = dict_nominal_cell_parameter[parameter_set][2]

        # scaling factor for power and energy from cell to system:
        # represents the number of individual cells used for the system
        self.__nominal_cap_cell_wh = dict_nominal_cell_parameter[parameter_set][0]
        self.__number_of_cells = capacity / self.__nominal_cap_cell_wh
        self.__number_of_serial_cells = voltage / self.__state.nominal_voltage
        self.__number_of_parallel_cells = self.__number_of_cells / self.__number_of_serial_cells

        # get relevant simulation variables from config
        self.__min_V = self.__parameter_values["Lower voltage cut-off [V]"]
        self.__max_V = self.__parameter_values["Upper voltage cut-off [V]"]
        self.__delta_t = config_general.timestep

        # first timestep to initialize model
        experiment = pybamm.Experiment([(f"Rest for {self.__delta_t} seconds")])
        self.__sim = pybamm.Simulation(self.__model, experiment=experiment, parameter_values=self.__parameter_values)
        self.__sol_ts = self.__sim.solve()
        # pre-set simulation for resting times, to speed these steps up significantly
        self.__sim_rest = pybamm.Simulation(self.__model, experiment=experiment,
                                            parameter_values=self.__parameter_values)

        # update and write initial state
        self.__last_time = self.__sol_ts["Time [h]"].data[-1] * 3600
        self.__cap = 0  # capacity in Ah for checkup cycles
        self.__x_0 = 0  # anode stoch. at 0% SOC based on checkup cycles
        self.__x_100 = 0  # anode stoch. at 100% SOC based on checkup cycles
        self.__r_mean = 0  # resistance in Ohm for checkup cycles
        self.__start_capacity = 0  # initialize start capacity
        self.__start_resistance = 0
        self.__update_state(self.__state, self.__sol_ts, config_general.start)
        self.__start_capacity = self.__cap  # determine start capacity based on first capacity checkup
        self.__start_resistance = self.__r_mean  # determine start resistance based on first capacity checkup
        self.__data_export.transfer_data(self.__state.to_export())
        self.__time_precision = 3  # used to start new CV phase
        self.__power_precision = 9  # formatting power; scientific notation "e-7" would lead to syntax error in PyBaMM
        self.__voltage_margin = 0.05  # voltage margin for determining CV phase voltage

    def distribute_and_run(self, time: float, current: float, voltage: float):
        """
        starts the update process of a data

        Parameters
        ----------
        time : current simulation time
        current : requested current for a data
        voltage : dc voltage for a data

        Returns
        -------

        """
        target_voltage = self.__sol_ts["Terminal voltage [V]"].data[-1]
        last_mean_power = self.__sol_ts.cycles[-1]["Terminal power [W]"].data.mean()
        power_per_cell = (-1)*current*voltage / self.__number_of_cells

        # determine experiment type (CP, CV or rest phase)
        if round(target_voltage, self.__voltage_CV_rounding) == self.__max_V and power_per_cell < 0:  # CV charge
            if abs(last_mean_power) < (self.__nominal_cap_cell_wh / 25):  # charge up to P /25 max in CV phase.
                exp_str = f"Rest for {self.__delta_t} seconds"
            else:
                exp_str = self._get_experiment_hold(self.__delta_t, self.__max_V)
        elif round(target_voltage, self.__voltage_CV_rounding) == self.__min_V and power_per_cell > 0:  # CV discharge
            if abs(last_mean_power) < (self.__nominal_cap_cell_wh / 25):  # discharge up to P /25 max in CV phase.
                exp_str = f"Rest for {self.__delta_t} seconds"
            else:
                exp_str = self._get_experiment_hold(self.__delta_t, self.__min_V)
        elif power_per_cell == 0:
            exp_str = f"Rest for {self.__delta_t} seconds"
        else:
            exp_str = self._get_experiment_power(power_per_cell, self.__delta_t, self.__min_V, self.__max_V)

        try:
            if self.__save_last_state_only:
                self.__sol_ts = self.__sol_ts.last_state
            if exp_str.startswith(f"Rest for {self.__delta_t} seconds"):
                try:
                    self.__sol_ts = self.__sim_rest.solve(calc_esoh=False, starting_solution=self.__sol_ts)
                except pybamm.SolverError:
                    print("Rest experiment failed. Attempting to rebuild model and solve again.")
                    exp = pybamm.Experiment([f"Rest for {self.__delta_t} seconds"])
                    self.__sim_rest = pybamm.Simulation(self.__model, experiment=exp,
                                                        parameter_values=self.__parameter_values,
                                                        solver=self.__solver, var_pts=self.__var_pts)
                    self.__sol_ts = self.__sim_rest.solve(calc_esoh=False, starting_solution=self.__sol_ts)
            else:
                experiment = pybamm.Experiment([exp_str])
                self.__sim = pybamm.Simulation(self.__model, experiment=experiment,
                                               parameter_values=self.__parameter_values,
                                               solver=self.__solver, var_pts=self.__var_pts)
                self.__sol_ts = self.__sim.solve(calc_esoh=False, starting_solution=self.__sol_ts)
        except pybamm.SolverError:
            print("PyBaMM model could not be solved for the current timestep. Simulation continues with CV phase.")

        expected_time = self.__last_time + self.__delta_t
        time_pybamm = self.__sol_ts["Time [h]"].data[-1] * 3600
        rest_time = round(expected_time - time_pybamm, self.__time_precision)

        if rest_time > 0:  # in case charge or discharge cut off voltage has been reached
            target_voltage = self.__sol_ts["Terminal voltage [V]"].data[-1]
            if target_voltage > self.__max_V - self.__voltage_margin:
                target_voltage = self.__max_V
            elif target_voltage < self.__min_V + self.__voltage_margin:
                target_voltage = self.__min_V

            experiment = self._get_experiment_hold(rest_time, round(target_voltage, self.__voltage_CV_rounding))
            self.__sim = pybamm.Simulation(self.__model, experiment=experiment,
                                           parameter_values=self.__parameter_values,
                                           solver=self.__solver, var_pts=self.__var_pts)
            self.__sol_ts = self.__sim.solve(calc_esoh=False, starting_solution=self.__sol_ts)
        # update and write state
        self.__update_state(self.__state, self.__sol_ts, time)
        self.__data_export.transfer_data(self.__state.to_export())
        self.__last_time = self.__sol_ts["Time [h]"].data[-1] * 3600

    def __update_state(self, state: LibPyBaMMState, pybamm_sol, time_simses: float):
        # one delta_t time shift between SimSES and PyBaMM due to initial 900s rest
        try:
            if self.__counter_checkup % self.__checkup_frequency == 0:
                # sometimes checkup experiment runs a "fresh model" (unsolved bug)
                # this occurs especially if the battery is "full" at the beginning of the experiment
                # therefore, only update SOH related values if checkup experiment solved as expected
                cap, x_100, x_0 = self.__checkup_c()
                if (cap < 0.999*self.__start_capacity or cap > 1.001*self.__start_capacity) or self.__counter_checkup == 0:
                    self.__cap, self.__x_100, self.__x_0= cap, x_100, x_0
                self.__counter_failed_checkup_c = 0
                r_mean = self.__checkup_r()
                if r_mean > self.__start_resistance*1.001 or self.__counter_checkup == 0:
                    self.__r_mean = r_mean
                self.__counter_failed_checkup_r = 0
        except pybamm.SolverError:
            # solve error occurs if the cell is already at >= 4.2V and charge to 4.2V is attempted during checkup
            self.__counter_failed_checkup_c += 1
            self.__counter_failed_checkup_r += 1
            print("SolveError during Checkup experiment. Consecutive failed Checkup Counter C & R: "
                  + str(self.__counter_failed_checkup_c) + "&" + str(self.__counter_failed_checkup_r))
        finally:
            self.__counter_checkup += 1
            state.time = time_simses
            # determine soc based on anode stoichiometry
            x = pybamm_sol["Negative electrode stoichiometry"].data[-1]
            soc = (x - self.__x_0) / (self.__x_100 - self.__x_0)

            # update state variables
            state.soc = soc
            state.soh = self.__cap / self.__start_capacity if self.__start_capacity != 0 else 1
            state.soe = soc * self.__nominal_cap_cell_wh * self.__number_of_cells * state.soh
            state.resistance_increase = self.__r_mean / self.__start_resistance - 1 if self.__start_resistance != 0 else 0
            print(f"Current SOC: {soc:.{4}f}, Current SOH: {state.soh:.{4}f}")

            # use average of power values from PyBaMM and last terminal voltage to get the current
            power_mean = pybamm_sol.cycles[-1]["Terminal power [W]"].data.mean()
            voltage_last = pybamm_sol["Terminal voltage [V]"].data[-1]
            current_mean = power_mean/voltage_last # to ensure right average power is used in simulations above
            state.voltage = voltage_last * self.__number_of_serial_cells
            state.voltage_open_circuit = voltage_last * self.__number_of_serial_cells
            state.current = (-1)*current_mean * self.__number_of_parallel_cells  # in SimSES, positive current means charging, in PyBaMM discharging
            state.temperature = pybamm_sol["Cell temperature [K]"].data[-1].mean()
            state.capacity = self.__cap * state.nominal_voltage * self.__number_of_cells  # in Wh
            state.resistance = self.__cap * self.__number_of_serial_cells / self.__number_of_cells  # in Ohm

            # degradation specific parameters
            state.lli = float(pybamm_sol.cycles[-1].cycle_summary_variables['Loss of lithium inventory [%]'])
            state.lam_ne = float(pybamm_sol.cycles[-1].cycle_summary_variables['Loss of active material in negative electrode [%]'])
            state.lam_pe = float(pybamm_sol.cycles[-1].cycle_summary_variables['Loss of active material in positive electrode [%]'])
            state.total_sei_thickness = float(pybamm_sol.cycles[-1]["X-averaged total SEI thickness [m]"].data[-1])
            state.li_plating_thickness = float(pybamm_sol.cycles[-1]["X-averaged lithium plating thickness [m]"].data[-1])
            state.anode_porosity = float(pybamm_sol.cycles[-1]["X-averaged negative electrode porosity"].data[-1])
            state.anode_porosity_min = float(np.min(pybamm_sol.cycles[-1]["Negative electrode porosity"].data))
            state.lli_sei = float(pybamm_sol.cycles[-1].cycle_summary_variables['Loss of lithium to SEI [mol]'])
            state.lli_sei_crack = float(pybamm_sol.cycles[-1].cycle_summary_variables['Loss of lithium to SEI on cracks [mol]'])
            state.lli_plating = float(pybamm_sol.cycles[-1].cycle_summary_variables['Loss of lithium to lithium plating [mol]'])
            state.lli_lam_ne += float(pybamm_sol.cycles[-1]["Loss of lithium due to loss of active material in negative electrode [mol]"].data[-1])
            state.lli_lam_pe += float(pybamm_sol.cycles[-1]["Loss of lithium due to loss of active material in positive electrode [mol]"].data[-1])
            state.max_charge_power = state.capacity * 2  # C-rate of 2
            state.max_discharge_power = state.capacity * 2

    def __checkup_c(self) -> (float, float, float):
        # checkup capacity
        t_rest = 60
        v_min = self.__min_V
        v_max = self.__max_V
        c_discharge = 0.2
        c_charge = 0.2
        plan_checkup_cap = [(
            f"Charge at {c_charge}C until {v_max} V",
            f"Hold at {v_max} V until C/25",
            f"Rest for {t_rest} minutes",
            f"Discharge at {c_discharge}C until {v_min} V",
            f"Rest for {t_rest} minutes",
            f"Charge at {c_charge}C until {v_max} V",
            f"Hold at {v_max} V until C/25",
            f"Rest for {t_rest} minutes",
            f"Discharge at {c_discharge}C until {v_min} V",
            f"Rest for {t_rest} minutes",
        )]
        experiment = pybamm.Experiment(plan_checkup_cap)
        param_checkup = self.__parameter_values.copy()
        # deactivate aging for checkups
        param_checkup.update(
            {"Exchange-current density for plating [A.m-2]": 0.00,
             "SEI kinetic rate constant [m.s-1]": 0.00,
             "Negative electrode LAM constant proportional term [s-1]": 0,
             "Positive electrode LAM constant proportional term [s-1]": 0}
            # Currently hard-coded for the following aging mechanisms: "SEI": "ec reaction limited",
            # "loss of active material": "stress-driven", "lithium plating": "irreversible"
        )
        sim = pybamm.Simulation(self.__model, experiment=experiment, parameter_values=param_checkup,
                                       solver=self.__solver, var_pts=self.__var_pts)
        sol_checkup_c = sim.solve(calc_esoh=False, starting_solution=self.__sol_ts.last_state)
        cap_checkup = sol_checkup_c.cycles[-1].steps[8]['Discharge capacity [A.h]'].data[-1]
        stoch_neg_min = min(sol_checkup_c.cycles[-1]["Negative electrode stoichiometry"].data)
        stoch_neg_max = max(sol_checkup_c.cycles[-1]["Negative electrode stoichiometry"].data)
        return cap_checkup, stoch_neg_max, stoch_neg_min

    def __checkup_r(self) -> float:
        param_checkup = self.__parameter_values.copy()
        # deactivate aging for checkups
        param_checkup.update(
            {"Exchange-current density for plating [A.m-2]": 0.00,
             "SEI kinetic rate constant [m.s-1]": 0.00,
             "Negative electrode LAM constant proportional term [s-1]": 0,
             "Positive electrode LAM constant proportional term [s-1]": 0}
            # Currently hard-coded for the following aging mechanisms: "SEI": "ec reaction limited",
            # "loss of active material": "stress-driven", "lithium plating": "irreversible"
        )
        # checkup resistance
        v_target = 3.6
        c_charge = 0.5
        plan_checkup_r = [(
            f"Charge at {c_charge}C until {v_target} V",
            f"Hold at {v_target} V until C/50",
            f"Rest for 120 minutes",
            f"Discharge at 0.25C for 10 s",
            f"Rest for 10 minutes",
            f"Charge at 0.25C for 10 s",
            f"Rest for 10 minutes",
            f"Discharge at 0.5C for 10 s",
            f"Rest for 10 minutes",
            f"Charge at 0.5C for 10 s",
            f"Rest for 10 minutes",
            f"Discharge at 0.75C for 10 s",
            f"Rest for 10 minutes",
            f"Charge at 0.75C for 10 s",
            f"Rest for 10 minutes",
            f"Rest for 120 minutes",
            #
        )]
        experiment = pybamm.Experiment(plan_checkup_r)
        sim = pybamm.Simulation(self.__model, experiment=experiment, parameter_values=param_checkup,
                                       solver=self.__solver, var_pts=self.__var_pts)
        sol_checkup_r = sim.solve(calc_esoh=False, starting_solution=self.__sol_ts.last_state)

        index_charge = [5, 9, 13]
        index_discharge = [3, 7, 11]
        resistances_charge = []
        resistances_discharge = []
        for idx, idx_charge in enumerate(index_charge):
            delta_v = sol_checkup_r.cycles[-1].steps[idx_charge]["Terminal voltage [V]"].data[-1] - \
                      sol_checkup_r.cycles[-1].steps[idx_charge - 1]["Terminal voltage [V]"].data[-1]
            current = sol_checkup_r.cycles[-1].steps[idx_charge]["Current [A]"].data.mean()
            resistances_charge.append(-1 * delta_v / current)
        for idx, idx_charge in enumerate(index_discharge):
            delta_v = sol_checkup_r.cycles[-1].steps[idx_charge]["Terminal voltage [V]"].data[-1] - \
                      sol_checkup_r.cycles[-1].steps[idx_charge - 1]["Terminal voltage [V]"].data[-1]
            current = sol_checkup_r.cycles[-1].steps[idx_charge]["Current [A]"].data.mean()
            resistances_discharge.append(-1 * delta_v / current)
        r_mean = np.mean(resistances_charge+resistances_discharge)
        return r_mean

    def _get_experiment_power(self, power, delta_t, min_V, max_V) -> str:
        if power < 0:
            experiment_str = f"Charge at {abs(power):.{self.__power_precision}f} W for {delta_t} seconds or until {max_V} V"
        elif power > 0:
            experiment_str = f"Discharge at {abs(power):.{self.__power_precision}f} W for {delta_t} seconds or until {min_V} V"
        else:
            experiment_str = f"Rest for {delta_t} seconds"
        return experiment_str

    # hold power at voltage
    def _get_experiment_hold(self, delta_t, voltage):
        experiment_str = f"Hold at {voltage} V for {delta_t:.{self.__time_precision}f} seconds"
        return experiment_str

    def wait(self):
        pass

    def get_auxiliaries(self) -> [Auxiliary]:
        return list()

    @property
    def state(self) -> StorageTechnologyState:
        return self.__state

    @property
    def volume(self) -> float:
        return 0

    @property
    def mass(self) -> float:
        raise NotImplementedError('Mass is not implemented in LibPybaMM yet')

    @property
    def surface_area(self) -> float:
        raise NotImplementedError('Surface area is not implemented in LibPybaMM yet')

    @property
    def specific_heat(self) -> float:
        raise NotImplementedError('Specific heat is not implemented in LibPybaMM yet')

    @property
    def convection_coefficient(self) -> float:
        raise NotImplementedError('Convection coefficient is not implemented in LibPybaMM yet')

    def get_system_parameters(self) -> dict:
        return dict()

    def close(self):
        """Closing all open resources"""
        self.__log.close()
        self.__sol_ts.plot(["Current [A]", "Terminal power [W]", "Terminal voltage [V]", "Negative electrode stoichiometry"])

        # save pybamm data
        full_name = self.__data_export.result_folder + "\pybamm_solution.pkl"
        self.__sol_ts.save(full_name)
        full_param_name = self.__data_export.result_folder + "\pybamm_parameter.txt"
        with open(full_param_name, 'w') as file:
            to_print = dict(self.__parameter_values)
            for key, value in to_print.items():
                file.write(f"{key}: {value}\n")