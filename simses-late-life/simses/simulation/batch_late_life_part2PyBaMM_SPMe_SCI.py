from configparser import ConfigParser

from simses.commons.config.generation.analysis import AnalysisConfigGenerator
from simses.commons.config.generation.simulation import SimulationConfigGenerator
from simses.simulation.batch_processing import BatchProcessing


class LateLifeBatchPart2(BatchProcessing):

    def __init__(self):
        super().__init__(do_simulation=True, do_analysis=True)

    def _setup_config(self) -> dict:
        config_paths = ["simulation.local_20230929_SCI_ParameterSetV5.ini"]

        # config set to be later passed to SimSES
        config_set: dict = dict()
        for config_name in config_paths:
            config_generator: SimulationConfigGenerator = SimulationConfigGenerator()
            config_generator.load_specific_config_full_name(config_name)
            conf = config_generator.get_config()
            conf["LIB_PYBAMM"]["MODEL_TYPE"] = "SPMe"
            name = config_name[-18:] + " - SPMe"
            config_set[name] = conf

            # config_generator: SimulationConfigGenerator = SimulationConfigGenerator()
            # config_generator.load_specific_config_full_name(config_name)
            # conf = config_generator.get_config()
            # conf["LIB_PYBAMM"]["MODEL_TYPE"] = "DFN"
            # name = config_name[-18:] + " - DFN"
            # config_set[name] = conf
        return config_set

    def _analysis_config(self) -> ConfigParser:
        config_name = "analysis.local_latelife_SCI.ini"
        config_generator: AnalysisConfigGenerator = AnalysisConfigGenerator()
        config_generator.load_specific_config_full_name(config_name)
        config_generator.print_results(False)
        config_generator.do_plotting(False)
        return config_generator.get_config()

    def clean_up(self) -> None:
        pass


if __name__ == "__main__":
    batch_processing: BatchProcessing = LateLifeBatchPart2()
    batch_processing.run()
    batch_processing.clean_up()
