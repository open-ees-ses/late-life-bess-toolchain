In this folder you can find several examples of how to 
use SimSES. 
To start one of the simulations, copy the file in your
main SimSES folder and name it "simulation.local.ini".

In general, SimSES uses the "simulation.defaults.ini" and 
overrides each value that is specified in the 
"simulation.local.ini"-file.

To edit one of the exemplary files, just edit it after
having copied it to your main SimSES folder.

The following examples are available:

**1. power_soc_follower**

In this example, the SimSES power and soc follower are used. In this strategy, 
the energy-management-system (EMS)/operation strategy follows a given AC power profile or
a given SOC profile. If users have a residual storage profile, they can follow this profile
with the PowerFollower. If they have a storage SOC profile, they can follow this profile
with the SocFollower. 
If the SocFollower ist used, SimSES calculates the required power to reach the desired SOC 
in each timestep.
The options that might be important for users in this strategy are explained in the ini-file.
In general ...
- the time properties can be chosen in the section [GENERAL]
- the EMS-strategy in [ENERGY_MANAGEMENT]
- the battery properties in [BATTERY]
- the storage system properties in [STORAGE_SYSTEM]
- and the profile properties in [PROFILE]

If users want to use their profile, they have to give the name of the profile in 
LOAD_PROFILE respectively SOC_PROFILE and drop their profile in the power profile direction
(POWER_PROFILE_DIR) respectively the technical profile direction (TECHNICAL_PROFILE_DIR).
The default directions are 
- ...\simses\data\profile\power and
- ...\simses\data\profile\technical

**2. thermal_simulation_residential_storage**

In this example, users can simulate the operation of a residential home storage system with PV, while observing its
thermal behavior.
The fields in the GENERAL section can be set to typical values.
``` 
[GENERAL]
...
``` 
In the ENERGY_MANAGEMENT section, two operation strategies are available as shown below. Please check the documentation
of each strategy for further details on the operation logic. We specify minimum and maximum permissible SOC values next.
``` 
[ENERGY_MANAGEMENT]
STRATEGY = ResidentialPvGreedy
;STRATEGY = ResidentialPvFeedInDamp
...
```
In the BATTERY section, the familiar parameters pertaining to the SOC, SOH, EOL criterion may be specified.
``` 
[BATTERY]
...
``` 
In the STORAGE_SYSTEM section, the parameters to specify the configuration of the system for simulation of the thermal 
are then specified. We specify here that our BESS  consists of 1 ACSystem with a power rating of 5 kW, an intermediate 
DC-Voltage of 333 V, and which does not have any housing type or a dedicated HVAC system connected to it. 
In this case, the system is installed indoors and a constant room temperature (= 25 °C) is assumed to be maintained.
The system is installed indoors and is not be exposed to direct sunlight.
As we are interested in the temperature evolution of our cells, we enable themral simulation.

Fields not discussed here (...) may be specified freely.
``` 
[STORAGE_SYSTEM]
STORAGE_SYSTEM_AC = 
    ac_system_1,5e3,333,acdc_1,no_housing,no_hvac
...
HOUSING =
    no_housing,NoHousing
HVAC =
    no_hvac,NoHeatingVentilationAirConditioning
...
AMBIENT_TEMPERATURE_MODEL = ConstantAmbientTemperature,25
SOLAR_IRRADIATION_MODEL = NoSolarIrradiationModel
THERMAL_SIMULATION = True
...
``` 
In the PROFILE section, we specify the load profile to be used for the household, with the type of scaling and specify
the value of the scaling factor. In this case, we scale the profile to correspond to an annual energy consumption of 
3.5 MWh. We also specify the PV generation profile to be used, followed by the scaling type and scaling factor for the 
generation profile (scaled by peak power, for a peak power value of 10 kW).
``` 
[PROFILE]
LOAD_PROFILE = SBAP_Household_Profile
GENERATION_PROFILE = SBAP_PV_EEN_Power_Munich_2014

LOAD_PROFILE_SCALING = Energy
LOAD_SCALING_FACTOR = 3.5e6

GENERATION_PROFILE_SCALING = Power
GENERATION_SCALING_FACTOR = 10e3
...
```
**3. thermal_simulation_peak_shaving**

In this example , we will take a look at how to setup a simulation for an industry-storage
providing peak shaving service . This file specifies the configuration for a 1 MW / 1.25 MWh BESS.

The fields in the GENERAL section can be set to typical values.
``` 
[GENERAL]
...
``` 
Now that the simulation period and sample time for our simulation has been specified, we specify which algorithm is to
be used for the Energy Management.
We look at the SimplePeakShaving algorithm in this example.
``` 
[ENERGY_MANAGEMENT]
STRATEGY = SimplePeakShaving
MIN_SOC = 0.0
MAX_SOC = 1.0
MAX_POWER = 2.5e6
``` 
Having settled for the energy management strategy of interest, we specify some basic battery parameters.
``` 
[BATTERY]
...
``` 
In the STORAGE_SYSTEM section, the parameters to specify the configuration of the system for simulation of the thermal 
are then specified. We specify the configuration details for our BESS at each level of the hierarchy.
We specify here that our BESS  consists of 1 ACSystem with a power rating of 1 MW, an intermediate DC-Voltage of 333 V,
and with a housing and a dedicated HVAC system connected to it. We now specify the type of Housing to be installed in our BESS. In this case, the system is installed outdoors in a
20 ft. container. Next we specify the type of HVAC to be installed in our BESS. In this case, the system is installed outdoors and the
shipping container has specialized HVAC unit.
In this case, the system is installed outdoors and a location-dependent ambient temperature is present.
Also being outdoors, the system is installed outdoors and is exposed to direct sunlight.
As we are interested in the temperature evolution of our cells under these conditions, we enable thermal simulation.
``` 
[STORAGE_SYSTEM]
STORAGE_SYSTEM_AC =
    ac_system_1,1e6,333,acdc_1,housing_1,hvac_1
...
HOUSING =
    housing_1,TwentyFtContainer,False,0.0,0.15,0.2
HVAC =
    hvac_1,FixCOPHeatingVentilationAirConditioning,30000,25.0
...
AMBIENT_TEMPERATURE_MODEL = LocationAmbientTemperature
SOLAR_IRRADIATION_MODEL = LocationSolarIrradiationModel
THERMAL_SIMULATION = True
...
``` 
In the PROFILE section, we specify the load profile to be used for the industry load, with the type of scaling and specify
the value of the scaling factor. In this case, we scale the profile to correspond to peak power value of 
3.5 MW. 
We also specify the location temperature and solar irradiation profiles to be used for the thermal simulation.
```
[PROFILE]
...
LOAD_PROFILE = SBAP_Industry_Input_Profiles_mix_public
...
AMBIENT_TEMPERATURE_PROFILE = berlin_temperature_1h
GLOBAL_HORIZONTAL_IRRADIATION_PROFILE = berlin_global_horizontal_irradiance_1h

LOAD_PROFILE_SCALING = Power
LOAD_SCALING_FACTOR = 3.5e6
```
**4. thermal_simulation_fcr_idm_stacked**

In this example config file, we will take a look at how to setup a simulation for a BESS providing
Frequency Containment Reserve (FCR) and participating in the Intraday Market (IDM) to restore its SOC to the range
stipulated by the reserve requirements for FCR.
This file specifies the configuration for a 1.35 MW / 1.35 MWh BESS

The fields in the GENERAL section can be set to typical values.
``` 
[GENERAL]
...
```

Now that the simulation period and sample time for our simulation has been specified, we specify which algorithm is to
be used for the Energy Management. Now that the simulation period and sample time for our simulation has been specified, we specify which algorithm is to
be used for the Energy Management.
We look at the FcrIdmRechargeStacked algorithm in this example.
```
[ENERGY_MANAGEMENT]
STRATEGY = FcrIdmRechargeStacked
MIN_SOC = 0.0
MAX_SOC = 1.0
POWER_FCR = 1.1e6
POWER_IDM = 0.25e6
SOC_SET = 0.52
FCR_RESERVE = 0.25
```
Having settled for the energy management strategy of interest, we specify some basic battery parameters.
``` 
[BATTERY]
...
``` 
In the STORAGE_SYSTEM section, the parameters to specify the configuration of the system for simulation of the thermal 
are then specified. We specify the configuration details for our BESS at each level of the hierarchy.
We specify here that our BESS  consists of 1 ACSystem with a power rating of 1.35 MW, an intermediate DC-Voltage of 333 V,
and with a housing and a dedicated HVAC system connected to it.
```
[STORAGE_SYSTEM]
STORAGE_SYSTEM_AC =
    ac_system_1,1.35e6,333,acdc_1,housing_1,hvac_1
...
HOUSING =
    housing_1,FortyFtContainer,False,0.0,0.15,0.2
HVAC =
    hvac_1,FixCOPHeatingVentilationAirConditioning,30000.0,25.0
...
AMBIENT_TEMPERATURE_MODEL = LocationAmbientTemperature
SOLAR_IRRADIATION_MODEL = LocationSolarIrradiationModel
THERMAL_SIMULATION = True
...
```
In the PROFILE section, we specify the frequency profile for the grid.
We also specify the location temperature and solar irradiation profiles to be used for the thermal simulation.
```
[PROFILE]
TECHNICAL_PROFILE_DIR = profile/technical/
FREQUENCY_PROFILE = SBAP_Frequency_2014

THERMAL_PROFILE_DIR = profile/thermal/
AMBIENT_TEMPERATURE_PROFILE = berlin_temperature_1h
GLOBAL_HORIZONTAL_IRRADIATION_PROFILE = berlin_global_horizontal_irradiance_1h
```

**5. ev_simulation**

In this example, users can simulate an electric vehicle (EV).
In addition to the other typical sections, such as [GENERAL], [BATTERY], [STORAGE SYSTEM], 
the following has to be applied:
```
[ENERGY_MANAGEMENT]
STRATEGY = ElectricVehicle
CHARGING_STRATEGY = Uncontrolled
# CHARGING_STRATEGY = Mean_power
# CHARGING_STRATEGY = Paused, 0.6
```

As of March 2023, the charging strategy can be 
- uncontrolled charging: **Uncontrolled**, 
- charging with medium power to reach 100% SOC at departure: **Mean_power** 
- and paused charging: e.g. **Paused, 0.6**. 

The paused charging strategy means that after arrival, the vehicle is charged to a 
threshold SOC. Afterwards the charging is paused. Shortly before the departure, the
vehicle is then charged to 100% SOC. The threshold SOC has to be defined in the name of the
charging strategy, e.g. "Paused, 0.6" -> 60% or "Paused, 0.85" -> 85%.

In comparison with stationary storage systems, EVs are not always available. Thus, SimSES
requires two profiles in the PROFILE section to simulate an EV:

- First, a load profile has to be given, that contains the load of all driving processes.
- Second, a binary profile that contains 0 whenever the vehicle is on the road or anywhere
else but not at home/ at depot/ plugged in. If, on the other hand, the EV is assumed to be 
plugged-in, the binary profile has to be 1. During times when the binary profile is 1, the 
charging strategy is applied in SimSES. The binary profile variable is BINARY_PROFILE, and 
it has to be put in the direction of the technical profiles.

```
[PROFILE]
POWER_PROFILE_DIR = profile/power/
LOAD_PROFILE = Your_Name_of_Load_Profile

TECHNICAL_PROFILE_DIR = profile/technical/
BINARY_PROFILE = Your_Name_of_Binary_Profile
```

If the load profile contains charging processes, SimSES neglects them and applies the chosen
charging strategy, when the binary vector's value is 1. If an EV profile is to be simulated 
that contains the driving and charging, the PowerFollower operation strategy can be used.

If users only have an SOC profile of the vehicle, they can use the operation strategy
"ElectricVehicleSOC" with the same defined charging strategies.

```
[ENERGY_MANAGEMENT]
STRATEGY = ElectricVehicleSOC
CHARGING_STRATEGY = Uncontrolled
# CHARGING_STRATEGY = Mean_power
# CHARGING_STRATEGY = Paused, 0.6
```


**6. semi_dynamic_multi_use**

This strategy allows a combination of the other strategies. 
The combination takes place semidynamically. This means that at the beginning, energy and 
power are allocated for each of the applications (in per unit; sum has to be one). 
Moreover, a ranking has to be defined giving the priority of the sub-applications.
In the following example, the battery storage system is used for Peak-Shaving (30% of power and energy; Prio 1) 
and self-consumption increase (70% of power and energy; Prio 2). In addition, the peak shaving limit is set to 80 kW.

```
[ENERGY_MANAGEMENT]
STRATEGY = SemiDynamicMultiUse
MAX_POWER = 80e3
MULTI_USE_STRATEGIES = ResidentialPvGreedy, SimplePeakShaving
ENERGY_ALLOCATION = 0.7, 0.3
POWER_ALLOCATION = 0.7, 0.3
RANKING = 2, 1
```

The following examplary profiles can be used for the multi-use simulation.
Users can, of course, also specify their own profiles.

```
[PROFILE]
POWER_PROFILE_DIR = profile/power/
load_profile = SBAP_Household_Profile
generation_profile = SBAP_PV_EEN_Power_Munich_2014
````

The simulation procedure is then that the individual applications are called up and the 
power required to be charged or discharged is determined in each case.
Power and energy limitations are considered depending on the allocation.
The power is then allocated in order of priority, so that lower-priority applications 
may not receive the desired power.
The simulation also automatically differentiates between behind the meter (BTM) and in front of the meter (FTM) applications. 
A higher priority BTM application can thus also draw power from the subsystem of a lower priority BTM application. 
This is not possible between BTM and FTM subsystems.