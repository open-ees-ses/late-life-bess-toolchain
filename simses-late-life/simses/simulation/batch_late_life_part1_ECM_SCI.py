from configparser import ConfigParser

from simses.commons.config.generation.analysis import AnalysisConfigGenerator
from simses.commons.config.generation.simulation import SimulationConfigGenerator
from simses.simulation.batch_processing import BatchProcessing


class LateLifeSCIBatchPart1(BatchProcessing):

    def __init__(self):
        super().__init__(do_simulation=True, do_analysis=True)

    def _setup_config(self) -> dict:
        # config set to be later passed to SimSES
        config_set: dict = dict()

        # defining parameters
        capacity_sci: float = 8800.0
        start_soc = 0.5

        # batteries = ["SamsungNCA21700", "SamsungNMC94AhLatelifestudy", "SonyLFP"]
        batteries = ["SamsungNCA21700", "SamsungNMC94AhLatelifestudy"]
        soh_values = [1, 0.95, 0.9, 0.85, 0.8, 0.75, 0.7, 0.65, 0.60, 0.55, 0.50]
        rinc_values = [0.6245/(0.9829-0.7380)*(1-soh) for soh in soh_values]  # based on SamsungNCA21700 aging study

        for battery in batteries:
            # for soh in soh_values:
            #     rinc = 0
            #     name = battery + "_SCI" + "_" + "{:.0f}".format(soh*100) + "_" + "{:3.0f}".format(rinc*100)
            #     config_set[name] = self.config_helper(capacity_sci, battery, start_soc, soh, rinc)
            # for rinc in rinc_values:
            #     soh = 1
            #     name = battery + "_SCI" + "_" + "{:.0f}".format(soh * 100) + "_" + "{:3.0f}".format(rinc * 100)
            #     config_set[name] = self.config_helper(capacity_sci, battery, start_soc, soh, rinc)
            for (soh,rinc) in zip(soh_values, rinc_values):
                name = "LateLifePart1_SCI" + "_" + "{:03.0f}".format(soh * 100) + "_" + "{:03.0f}".format(rinc * 100) + "_" + battery
                config_set[name] = self.config_helper(capacity_sci, battery, start_soc, soh, rinc)
        return config_set

    @staticmethod
    def config_helper(capacity_sci, battery, start_soc, soh_value, resistance_increase):
        config_name = "simulation.local_latelife_part1_SCI.ini"
        config_generator: SimulationConfigGenerator = SimulationConfigGenerator()
        config_generator.load_specific_config_full_name(config_name)
        config_generator.clear_storage_technology()
        config_generator.add_lithium_ion_battery(capacity=capacity_sci, cell_type=battery,
                                                                  start_soc=start_soc, start_soh=soh_value)
        conf = config_generator.get_config()
        conf["BATTERY"]["START_RESISTANCE_INC"] = "{:.2f}".format(resistance_increase)
        # config_generator.show()
        return conf

    def _analysis_config(self) -> ConfigParser:
        config_name = "analysis.local_latelife_SCI.ini"
        config_generator: AnalysisConfigGenerator = AnalysisConfigGenerator()
        config_generator.load_specific_config_full_name(config_name)
        config_generator.print_results(False)
        config_generator.do_plotting(False)
        return config_generator.get_config()

    def clean_up(self) -> None:
        pass


if __name__ == "__main__":
    batch_processing: BatchProcessing = LateLifeSCIBatchPart1()
    batch_processing.run()
    batch_processing.clean_up()
