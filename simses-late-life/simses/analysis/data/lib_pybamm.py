import pandas
from simses.analysis.data.abstract_data import Data
from simses.commons.config.simulation.general import GeneralSimulationConfig
from simses.commons.state.technology.lib_pybamm import LibPyBaMMState


class LibPyBaMMData(Data):
    """
    Provides time series data from LibPyBaMMState
    """

    def __init__(self, config: GeneralSimulationConfig, data: pandas.DataFrame):
        super().__init__(config, data)

    @property
    def id(self) -> str:
        return str(int(self._get_first_value(LibPyBaMMState.SYSTEM_AC_ID))) + '.' + \
               str(int(self._get_first_value(LibPyBaMMState.SYSTEM_DC_ID)))

    @property
    def time(self):
        return self._get_data(LibPyBaMMState.TIME)

    @property
    def power(self):
        return self.voltage * self.current

    @property
    def current(self):
        return self._get_data(LibPyBaMMState.CURRENT)

    @property
    def voltage(self):
        return self._get_data(LibPyBaMMState.VOLTAGE)

    @property
    def dc_power(self):
        return self.power

    @property
    def energy_difference(self):
        # initial_soc = self._get_first_value(LibPyBaMMState.SOC)
        # last_soc = self._get_last_value(LibPyBaMMState.SOC)
        # initial_capacity = float(self.capacity[0])
        # last_capacity = float(self.capacity[-1])
        # return last_soc * last_capacity - initial_soc * initial_capacity
        initial_soe = float(self.soe[0])
        last_soe = float(self.soe[-1])
        return last_soe - initial_soe

    @property
    def soe(self):
        return self._get_data(LibPyBaMMState.SOE) / 1000.0

    @property
    def soc(self):
        return self._get_data(LibPyBaMMState.SOC)

    @property
    def capacity(self):
        return self._get_data(LibPyBaMMState.CAPACITY) / 1000.0

    @property
    def nominal_voltage(self):
        return self._get_data(LibPyBaMMState.NOMINAL_VOLTAGE)[0]

    @property
    def state_of_health(self):
        return self._get_data(LibPyBaMMState.SOH)

    @property
    def lli(self):
        return self._get_data(LibPyBaMMState.LLI)

    @property
    def lli_sei(self):
        return self._get_data(LibPyBaMMState.LLI_SEI)

    @property
    def lli_sei_crack(self):
        return self._get_data(LibPyBaMMState.LLI_SEI_CRACK)

    @property
    def lli_lam_ne(self):
        return self._get_data(LibPyBaMMState.LLI_LAM_NE)

    @property
    def lli_lam_pe(self):
        return self._get_data(LibPyBaMMState.LLI_LAM_PE)

    @property
    def lli_plating(self):
        return self._get_data(LibPyBaMMState.LLI_PLATING)

    @property
    def lam_ne(self):
        return self._get_data(LibPyBaMMState.LAM_NE)

    @property
    def lam_pe(self):
        return self._get_data(LibPyBaMMState.LAM_PE)

    @property
    def total_sei_thickness(self):
        return self._get_data(LibPyBaMMState.TOTAL_SEI_THICKNESS)

    @property
    def li_plating_thickness(self):
        return self._get_data(LibPyBaMMState.LI_PLATING_THICKNESS)

    @property
    def anode_porosity(self):
        return self._get_data(LibPyBaMMState.ANODE_POROSITY)

    @property
    def anode_porosity_min(self):
        return self._get_data(LibPyBaMMState.ANODE_POROSITY_MIN)

    @property
    def storage_fulfillment(self):
        return self._get_data(LibPyBaMMState.FULFILLMENT)

    @property
    def temperature(self):
        return self._get_data(LibPyBaMMState.TEMPERATURE)

    @property
    def resistance(self):
        return self._get_data(LibPyBaMMState.INTERNAL_RESISTANCE)

    @property
    def resistance_increase(self):
        return self._get_data(LibPyBaMMState.RESISTANCE_INCREASE)

    @classmethod
    def get_system_data(cls, path: str, config: GeneralSimulationConfig) -> list:
        system_data: [pandas.DataFrame] = cls._get_system_data_for(path, LibPyBaMMState, LibPyBaMMState.TIME,
                                                                   LibPyBaMMState.SYSTEM_AC_ID, LibPyBaMMState.SYSTEM_DC_ID)
        res: [LibPyBaMMData] = list()
        for data in system_data:
            res.append(LibPyBaMMData(config, data))
        return res


