import numpy as np

from simses.analysis.data.lib_pybamm import LibPyBaMMData
from simses.analysis.data.lithium_ion import LithiumIonData
from simses.analysis.evaluation.plotting.axis import Axis
from simses.analysis.evaluation.plotting.plotly_plotting import PlotlyPlotting
from simses.analysis.evaluation.plotting.plotter import Plotting
from simses.analysis.evaluation.result import EvaluationResult, Description, Unit
from simses.analysis.evaluation.technical.technical_evaluation import TechnicalEvaluation
from simses.commons.config.analysis.general import GeneralAnalysisConfig
from simses.commons.config.simulation.battery import BatteryConfig
from simses.commons.state.technology.lib_pybamm import LibPyBaMMState


class LibPyBaMMTechnicalEvaluation(TechnicalEvaluation):

    __title_overview = 'SOC, internal resistance, temperature and capacity'
    __title_current = 'Current and voltage'
    __title_degradation = 'Degradation of capacity and increase of internal resistance'

    def __init__(self, data: LibPyBaMMData, config: GeneralAnalysisConfig, battery_config: BatteryConfig, path: str):
        super().__init__(data, config)
        title_extension: str = ' for lithium-ion system ' + self.get_data().id
        self.__title_overview += title_extension
        self.__title_current += title_extension
        self.__title_degradation += title_extension
        self.__result_path = path

    def evaluate(self):
        super().evaluate()
        self.append_result(EvaluationResult(Description.Technical.EQUIVALENT_FULL_CYCLES, Unit.NONE, self.equivalent_full_cycles))
        self.append_result(EvaluationResult(Description.Technical.DEPTH_OF_DISCHARGE, Unit.PERCENTAGE, self.depth_of_discharges))
        self.append_result(EvaluationResult(Description.Technical.STORAGE_TECHNOLOGY_TEMPORAL_UTILIZATION, Unit.NONE, self.storage_technology_temporal_utilization))
        self.append_result((EvaluationResult(Description.Technical.C_RATE_CHARGING, Unit.ONE_PER_HOUR, self.c_rate_mean_charge)))
        self.append_result((EvaluationResult(Description.Technical.C_RATE_DISCHARGING, Unit.ONE_PER_HOUR, self.c_rate_mean_discharge)))

        # self.append_result(EvaluationResult(Description.Technical.ENERGY_THROUGHPUT, Unit.KWH, self.energy_throughput))
        # self.append_time_series(LibPyBaMMState.VOLTAGE, self.get_data().voltage)
        self.print_results()

    @property
    def c_rate_mean_charge(self) -> float:

        """
        Calculates the average c-rate while charging

        Parameters
        ----------
            data : simulation results

        Returns
        -------
        float:
            mean c-rate when charging
        """
        data: LithiumIonData = self.get_data()
        charge_current_mean = np.mean(data.current[data.current > 0])
        battery_energy = 1000*data.initial_capacity # in Wh

        nominal_voltage = float(data.nominal_voltage)
        battery_capacity_ah = battery_energy/nominal_voltage
        c_rate_charging = charge_current_mean / battery_capacity_ah

        charge_power_mean = np.mean(data.power[data.power > 0])
        e_rate_charging = charge_power_mean/battery_energy # based on power and energy instead of current and capacity

        return c_rate_charging

    @property
    def c_rate_mean_discharge(self) -> float:

        """
        Calculates the average c-rate while discharging

        Parameters
        ----------
            data : simulation results

        Returns
        -------
        float:
            mean c-rate when discharging
        """
        data: LithiumIonData = self.get_data()
        discharge_current_mean = np.mean(data.current[data.current < 0])
        battery_energy = 1000*data.initial_capacity # in Wh

        nominal_voltage = data.nominal_voltage
        battery_capacity_ah = battery_energy / nominal_voltage
        c_rate_discharging = -discharge_current_mean / battery_capacity_ah

        discharge_power_mean = np.mean(data.power[data.power < 0])
        e_rate_discharging = -discharge_power_mean/battery_energy # based on power and energy instead of current and capacity

        return c_rate_discharging


    def plot(self) -> None:
        self.__plot_overview()
        self.__plot_current_voltage()
        self.__plot_degradation_overview()
        self.__plot_lli_overview()
        self.__plot_thickness_porosity()

    def __plot_overview(self) -> None:
        data: LibPyBaMMData = self.get_data()
        plot: Plotting = PlotlyPlotting(title=self.__title_overview, path=self.__result_path)
        xaxis: Axis = Axis(data=Plotting.format_time(data.time), label=LibPyBaMMState.TIME)
        yaxis: [Axis] = list()
        yaxis.append(Axis(data=data.soc, label=LibPyBaMMState.SOC, color=PlotlyPlotting.Color.SOC_BLUE,
                          linestyle=PlotlyPlotting.Linestyle.SOLID))
        yaxis.append(Axis(data=data.capacity * 1000, label=LibPyBaMMState.CAPACITY,
                          color=PlotlyPlotting.Color.SOH_GREEN, linestyle=PlotlyPlotting.Linestyle.SOLID))
        yaxis.append(Axis(data=data.resistance_increase, label=LibPyBaMMState.RESISTANCE_INCREASE,
                          color=PlotlyPlotting.Color.RESISTANCE_BLACK, linestyle=PlotlyPlotting.Linestyle.SOLID))
        yaxis.append(Axis(data=data.state_of_health, label=LibPyBaMMState.SOH,
                          color=PlotlyPlotting.Color.HEAT_ORANGE, linestyle=PlotlyPlotting.Linestyle.SOLID))
        plot.subplots(xaxis=xaxis, yaxis=yaxis)
        self.extend_figures(plot.get_figures())

    def __plot_degradation_overview(self) -> None:
        data: LibPyBaMMData = self.get_data()
        plot: Plotting = PlotlyPlotting(title="Loss components", path=self.__result_path)
        xaxis: Axis = Axis(data=Plotting.format_time(data.time), label=LibPyBaMMState.TIME)
        yaxis: [Axis] = list()
        yaxis.append(Axis(data=data.lli, label=LibPyBaMMState.LLI,
                          color=PlotlyPlotting.Color.BLUE, linestyle=PlotlyPlotting.Linestyle.SOLID))
        yaxis.append(Axis(data=data.lam_ne, label=LibPyBaMMState.LAM_NE,
                          color=PlotlyPlotting.Color.ANODE_GREEN, linestyle=PlotlyPlotting.Linestyle.SOLID))
        yaxis.append(Axis(data=data.lam_pe, label=LibPyBaMMState.LAM_PE,
                          color=PlotlyPlotting.Color.CATHODE_PINK, linestyle=PlotlyPlotting.Linestyle.SOLID))
        plot.lines(xaxis=xaxis, yaxis=yaxis)
        self.extend_figures(plot.get_figures())

    def __plot_thickness_porosity(self) -> None:
        data: LibPyBaMMData = self.get_data()
        plot: Plotting = PlotlyPlotting(title="Layer thickness and porosity", path=self.__result_path)
        xaxis: Axis = Axis(data=Plotting.format_time(data.time), label=LibPyBaMMState.TIME)
        yaxis: [Axis] = list()
        yaxis.append(Axis(data=data.total_sei_thickness, label=LibPyBaMMState.TOTAL_SEI_THICKNESS,
                          color=PlotlyPlotting.Color.HEAT_ORANGE, linestyle=PlotlyPlotting.Linestyle.SOLID))
        yaxis.append(Axis(data=data.li_plating_thickness, label=LibPyBaMMState.LI_PLATING_THICKNESS,
                          color=PlotlyPlotting.Color.RESISTANCE_BLACK, linestyle=PlotlyPlotting.Linestyle.SOLID))
        yaxis.append(Axis(data=data.anode_porosity, label=LibPyBaMMState.ANODE_POROSITY,
                          color=PlotlyPlotting.Color.RESISTANCE_BLACK, linestyle=PlotlyPlotting.Linestyle.DASHED))
        yaxis.append(Axis(data=data.anode_porosity_min, label=LibPyBaMMState.ANODE_POROSITY_MIN,
                          color=PlotlyPlotting.Color.RESISTANCE_BLACK, linestyle=PlotlyPlotting.Linestyle.DASHED))
        plot.lines(xaxis, yaxis, [2,3])
        self.extend_figures(plot.get_figures())

    def __plot_lli_overview(self) -> None:
        data: LibPyBaMMData = self.get_data()
        plot: Plotting = PlotlyPlotting(title="LLI overview", path=self.__result_path)
        xaxis: Axis = Axis(data=Plotting.format_time(data.time),  label=LibPyBaMMState.TIME)
        yaxis: [Axis] = list()
        yaxis.append(Axis(data=data.lli_sei, label=LibPyBaMMState.LLI_SEI, color=PlotlyPlotting.Color.RED,
                          linestyle=PlotlyPlotting.Linestyle.SOLID))
        yaxis.append(Axis(data=data.lli_sei_crack, label=LibPyBaMMState.LLI_SEI_CRACK, color=PlotlyPlotting.Color.HEAT_ORANGE,
                          linestyle=PlotlyPlotting.Linestyle.SOLID))
        yaxis.append(Axis(data=data.lli_plating, label=LibPyBaMMState.LLI_PLATING, color=PlotlyPlotting.Color.BLUE,
                          linestyle=PlotlyPlotting.Linestyle.SOLID))
        yaxis.append(Axis(data=data.lli_lam_ne, label=LibPyBaMMState.LLI_LAM_NE, color=PlotlyPlotting.Color.ANODE_GREEN,
                          linestyle=PlotlyPlotting.Linestyle.SOLID))
        yaxis.append(Axis(data=data.lli_lam_pe, label=LibPyBaMMState.LLI_LAM_PE, color=PlotlyPlotting.Color.CATHODE_PINK,
                          linestyle=PlotlyPlotting.Linestyle.SOLID))
        plot.lines(xaxis=xaxis, yaxis=yaxis)
        self.extend_figures(plot.get_figures())

    def __plot_current_voltage(self) -> None:
        data: LibPyBaMMData = self.get_data()
        plot: Plotting = PlotlyPlotting(title=self.__title_current, path=self.__result_path)
        xaxis: Axis = Axis(data=Plotting.format_time(data.time), label=LibPyBaMMState.TIME)
        yaxis: [Axis] = list()
        yaxis.append(Axis(data=data.current, label=LibPyBaMMState.CURRENT,
                          color=PlotlyPlotting.Color.RESISTANCE_BLACK, linestyle=PlotlyPlotting.Linestyle.SOLID))
        yaxis.append(Axis(data=data.voltage, label=LibPyBaMMState.VOLTAGE,
                          color=PlotlyPlotting.Color.TEMPERATURE_RED, linestyle=PlotlyPlotting.Linestyle.SOLID))
        plot.lines(xaxis=xaxis, yaxis=yaxis, secondary=[1])
        self.extend_figures(plot.get_figures())

    # @property
    # def capacity_remaining(self) -> float:
    #     data: LithiumIonData = self.get_data()
    #     return data.state_of_health[-1] * 100
